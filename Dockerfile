FROM linuxserver/nextcloud

# nextcloud-inotifyscan
# https://github.com/Blaok/nextcloud-inotifyscan
RUN apk add --no-cache --update python inotify-tools && \
    mkdir -p /etc/services.d/inotifyscan && \
    ln -s /config/www/nextcloud/occ /occ
ADD ./inotifyscan /etc/services.d/inotifyscan/run
RUN chmod +x /etc/services.d/inotifyscan/run